module QuotedTextParser where

import Parser
import BasicParser

noneOf :: [Char] -> Parser Char
noneOf t = satisfy (`notElem` t)

oneOf :: [Char] -> Parser Char
oneOf t = satisfy (`elem` t)

escape :: Parser String
escape = do
    d <- char '\\'
    c <- oneOf "\\\"0nrvtbf" -- all the characters which can be escaped
    pure [d, c]

nonEscape :: Parser Char
nonEscape = noneOf "\\\"\0\n\r\v\t\b\f"

character :: Parser String
character = fmap return nonEscape <|> escape

quotedTextP :: Parser String
quotedTextP = do
    q1  <- char '"'
    s   <- many character
    q2  <- char '"'
    pure $ concat s