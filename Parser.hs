module Parser where

-- Reference: https://youtu.be/LeoDCiu_GB0

newtype Parser a = Parser { runParser :: String -> Maybe (a, String) }

instance Functor Parser where
    fmap f (Parser x) = Parser $ \s -> do
        (x', s') <- x s
        return (f x', s')

instance Applicative Parser where
    pure x = Parser $ \s -> Just (x,s)

    (Parser f) <*> (Parser x) = Parser $ \s -> do
        (f', s1) <- f s
        (x', s2) <- x s1
        return (f' x', s2)

instance Monad Parser where
    (Parser x) >>= f = Parser $ \s -> do
        (x', s') <- x s
        runParser (f x') s'

instance MonadFail Parser where
    fail _ = Parser $ const Nothing

instance Alternative Parser where
    empty = fail ""
    (Parser x) <|> (Parser y) = Parser $ \s -> do
        case x s of
            Just x  -> Just x
            Nothing -> y s

class (Applicative f) => Alternative f where
    empty :: f a
    (<|>) :: f a -> f a -> f a
    some  :: f a -> f [a]
    some v = some_v
        where
            many_v = some_v <|> pure []
            some_v = (:) <$> v <*> many_v

    many  :: f a -> f [a]
    many v = many_v
        where
            many_v = some_v <|> pure []
            some_v = (:) <$> v <*> many_v
