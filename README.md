![image](https://raw.githubusercontent.com/abranhe/programming-languages-logos/master/src/haskell/haskell.svg?width=10&height=10)

# Parser Combinator Study

This repository is for midterm assignment of Functional Programming Course (CSCE604123).

# Author
Iqrar Agalosi Nureyza (1806204902)

# Basic Parser Demo
## Char Parser
Char Parser is the most basic parser. It can parse the single character in a string.
```haskell
char :: Char -> Parser Char
char c = satisfy (==c)
```
We can run it by calling `runParser` command which has defined in `Parser.hs`.
```bash
ghci> charF = char 'f'
ghci> runParser charF "fasilkom"
Just ('f',"asilkom")
ghci> runParser charF "Fasilkom"
Nothing
```
The single character is case sensitive, so we can use `<|>` to match the upper case or lower case. 
```bash
ghci> charF = char 'f' <|> char 'F'
ghci> runParser charF "Fasilkom"   
Just ('F',"asilkom")
ghci> runParser charF "fasilkom"
Just ('f',"asilkom")
```

## Digit Parser
Same as above, this parser can parse a single digit.
```haskell
digit :: Int -> Parser Char
digit d = satisfy (==dd)
    where
        dd = intToDigit d
```

```bash
ghci> digit8 = digit 8
ghci> runParser digit "800703285"
ghci> runParser digit8 "800703285"
Just ('8',"00703285")
ghci> runParser digit8 "99999"    
Nothing
```

## String Parser
String parser can match particular string we input. By using `<$>` functor as an infix `fmap` operator and `<*>` as a sequential application functor, we can implement a simple string parser.
```haskell
string :: String -> Parser String
string ""     = pure ""
string (c:cs) = (:) <$> char c <*> string cs
```

```bash
ghci> stringP = string "Iqrar"
ghci> runParser stringP "Iqrar Agalosi Nureyza"
Just ("Iqrar"," Agalosi Nureyza")
ghci> runParser stringP "My name is Iqrar Agalosi Nureyza"
Nothing
```

## Alphanumeric Parser
Using `isAlphaNum` function provided by `Data.Char` module is quite handy when handling a case when you need to parse alphanumeric character. Much better than using digit parser and char parser.
```haskell
alphaNum :: Parser Char 
alphaNum = satisfy isAlphaNum
```

```bash
ghci> runParser alphaNum "123a"
Just ('1',"23a")
ghci> runParser alphaNum "a123" 
Just ('a',"123")
ghci> runParser alphaNum "-123"
Nothing
```
## Space Parser
parsing space like newline (`\n`), tab (`\t`), and carriage return (`\r`).
```haskell
space :: Parser Char
space = char ' ' <|> char '\t' <|> char '\n' <|> char '\r'
```

```bash
ghci> runParser space " abc"  
Just (' ',"abc")
ghci> runParser space "\nabc"
Just ('\n',"abc")
ghci> runParser space "\tabc"
Just ('\t',"abc")
```

# Custom Parser Combinator Demo
## 1. Phone Number Parser
### Problem
We have a string of phone number contains 12 digits, but each 4 digits will seperate by dash (-) or space character. We want to retrieve the number and ignore the non-number character.

### Input
`7812-8904-2752` or `7812 8904 2752`

### Expected Parsing
`781289042752`

### How to solve?
By using basic parser to parse dash character and space character, we can ignore the character. We also make new parser called `fourDigits` which can be used to match four digit numbers.
```haskell
fourDigits :: Parser String
fourDigits = do
    a <- digit 0 <|> digit 1 <|> digit 2 <|> digit 3 <|> digit 4 <|> digit 5 <|> digit 6
                 <|> digit 7 <|> digit 8 <|> digit 9
    b <- digit 0 <|> digit 1 <|> digit 2 <|> digit 3 <|> digit 4 <|> digit 5 <|> digit 6
                 <|> digit 7 <|> digit 8 <|> digit 9
    c <- digit 0 <|> digit 1 <|> digit 2 <|> digit 3 <|> digit 4 <|> digit 5 <|> digit 6
                 <|> digit 7 <|> digit 8 <|> digit 9
    d <- digit 0 <|> digit 1 <|> digit 2 <|> digit 3 <|> digit 4 <|> digit 5 <|> digit 6
                 <|> digit 7 <|> digit 8 <|> digit 9
    pure [a,b,c,d]
```
The implementation above isn't optimal yet and can be refactored. But I'm still studying it to figure out how to refactor them.
And finally we came up with the solution below. At the end of the code, we use `++` operator to concat the string.
```haskell
phoneNumberP :: Parser String 
phoneNumberP = do
    aa <- fourDigits
    bb <- dash <|> space
    cc <- fourDigits
    dd <- dash <|> space
    ee <- fourDigits
    pure $ aa ++ cc ++ ee
```

```bash
ghci> runParser phoneNumberP "0823-8866-0001"
Just ("082388660001","")
ghci> runParser phoneNumberP "0823 8866 0001"
Just ("082388660001","")
```

## 2. Quoted Text Parser
### Problem
Given a quoted string, we need to parse the string and get the string only without the quotation mark (`"`). In this case, we need to care about the prefix and postfix of (`"`) and then parse all the string input between two quotation marks. Note that the string between them can be anything except the quotation mark itself.
### Input
`"Hi, my name's Iqrar!"`

### Expected Parsing
`Hi, my name's Iqrar!`

### How to solve?
In this problem we implemented a helper function like `noneOf` to check if a char is not in a string and `oneOf` to check whether a char is in a string or not.
```haskell
noneOf :: [Char] -> Parser Char
noneOf t = satisfy (`notElem` t)

oneOf :: [Char] -> Parser Char
oneOf t = satisfy (`elem` t)
```
Those functions are used in `escape` and `nonEscape` function. They can be useful to handle the string that contains a character that usually be escaped using backslash (`\`) such as: `\t`, `\b`, etc. After that, we combine those functions into one parser called `character` and use it in our main parser called `quotedTextP`.
```haskell
nonEscape :: Parser Char
nonEscape = noneOf "\\\"\0\n\r\v\t\b\f"

character :: Parser String
character = fmap return nonEscape <|> escape

quotedTextP :: Parser String
quotedTextP = do
    q1  <- char '"'
    s   <- many character
    q2  <- char '"'
    pure $ concat s
```
Note that we use `many` function to indicate that there will be at least one character or more instead of single char only. `many` function is implemented in `Parser.hs`. And finally, we concat them because `s` is a list of character.

```bash
ghci> runParser quotedTextP "\"Hi, my name's Iqrar!\""
Just ("Hi, my name's Iqrar!","")
ghci> runParser quotedTextP "\"\""                    
Just ("","")
```

---

# References
- https://youtu.be/LeoDCiu_GB0
- https://github.com/tsoding/haskell-json/blob/master/Main.hs
- https://github.com/ivan-m/CanFP-Parsing
- https://lettier.github.io/parsing-with-haskell-parser-combinators/#parser-combinator