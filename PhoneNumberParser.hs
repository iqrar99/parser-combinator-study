module PhoneNumberParser where

import Parser
import BasicParser

dash :: Parser Char 
dash = char '-'

fourDigits :: Parser String
fourDigits = do
    a <- digit 0 <|> digit 1 <|> digit 2 <|> digit 3 <|> digit 4 <|> digit 5 <|> digit 6
                 <|> digit 7 <|> digit 8 <|> digit 9
    b <- digit 0 <|> digit 1 <|> digit 2 <|> digit 3 <|> digit 4 <|> digit 5 <|> digit 6
                 <|> digit 7 <|> digit 8 <|> digit 9
    c <- digit 0 <|> digit 1 <|> digit 2 <|> digit 3 <|> digit 4 <|> digit 5 <|> digit 6
                 <|> digit 7 <|> digit 8 <|> digit 9
    d <- digit 0 <|> digit 1 <|> digit 2 <|> digit 3 <|> digit 4 <|> digit 5 <|> digit 6
                 <|> digit 7 <|> digit 8 <|> digit 9
    pure [a,b,c,d]

phoneNumberP :: Parser String 
phoneNumberP = do
    aa <- fourDigits
    bb <- dash <|> space
    cc <- fourDigits
    dd <- dash <|> space
    ee <- fourDigits
    pure $ aa ++ cc ++ ee
