module BasicParser where

import Parser
import Data.Char (isDigit, intToDigit, isAlphaNum)

satisfy :: (Char -> Bool) -> Parser Char
satisfy predicate = Parser satisfyC
    where
        satisfyC []                   = Nothing
        satisfyC (x:xs) | predicate x = Just (x,xs)
                        | otherwise   = Nothing

char :: Char -> Parser Char
char c = satisfy (==c)

digit :: Int -> Parser Char
digit d = satisfy (==dd)
    where
        dd = intToDigit d

space :: Parser Char
space = char ' ' <|> char '\t' <|> char '\n' <|> char '\r'

string :: String -> Parser String
string ""     = pure ""
string (c:cs) = (:) <$> char c <*> string cs

alphaNum :: Parser Char 
alphaNum = satisfy isAlphaNum